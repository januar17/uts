<?php 
include 'database.php';
$db = new database();
?>
<h1>CRUD OOP PHP</h1>
<h3>Data User</h3>

<a href="input.php">Input Data</a>
<table border="1">
	<tr>
		<th>No</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Username</th>
		<th>email</th>
	</tr>
	<?php
	$no = 1;
	foreach($db->tampil_data() as $x){
	?>
	<tr>
		<td><?php echo $x['id']; ?></td>
		<td><?php echo $x['first_name']; ?></td>
		<td><?php echo $x['last_name']; ?></td>
		<td><?php echo $x['username']; ?></td>
		<td><?php echo $x['email']; ?></td>
		<td>
			<a href="edit.php?id=<?php echo $x['id']; ?>&aksi=edit">Edit</a>
			<a href="proses.php?id=<?php echo $x['id']; ?>&aksi=hapus">Hapus</a>			
		</td>
	</tr>
	<?php 
	}
	?>
</table>